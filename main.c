/*
** main.c for Allum1 in /home/blackbird/rendu/Allum1
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Sun Feb 16 18:40:09 2014 romaric
** Last update Wed Feb 19 12:04:32 2014 romaric
*/

#include "./headers/allum1.h"

extern char **environ;

int	main(void)
{
  struct termios	t;
  int			*alum;

  alum = xmalloc(5 * sizeof(int*));
  auxmain(environ);
  iniint(&alum);
  xtcgetattr(0, &t);
  t.c_cc[VMIN] = 1;
  t.c_cc[VTIME] = 0;
  myclear();
  xtcsetattr (0, 0, &t);
  loopgame(alum);
  return (0);
}

void	iniint(int **alum)
{
  int	x;
  int	val;

  val = 1;
  x = 0;
  while (x <= 5)
    {
      (*alum)[x] = val;
      val = val + 2;
      x++;
    }
}

void    auxmain(char **env)
{
  char  *term;

  if (*env != NULL)
    {
      term = checterm(env);
      xtgetent(term);
    }
  else
    exit(EXIT_FAILURE);
}

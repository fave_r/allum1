##
## Makefile for Allum1 in /home/blackbird/rendu/Allum1
##
## Made by romaric
## Login   <fave_r@epitech.net>
##
## Started on  Sun Feb 16 18:42:46 2014 romaric
## Last update Sun Feb 16 19:02:20 2014 romaric
##

CC=	gcc

RM=	rm -f

CFLAGS	+=	-Wextra -Wall -Werror

NAME=	allum1

SRCS=	main.c \
	./utils/checkterm.c \
	./utils/char.c \
	./utils/my_strncmp.c \
	./utils/xfunction.c \
	./game/display.c \
	./game/read.c \
	./utils/my_getnbr.c \
	./IA/IA.c \
	./utils/myclear.c

OBJS=	$(SRCS:.c=.o)

LDFLAGS	+= -lncurses

all:	$(NAME)

$(NAME):	$(OBJS)
		$(CC) $(OBJS) -o $(NAME) $(LDFLAGS)

clean:
	$(RM) $(OBJS)

fclean:	clean
	$(RM) $(NAME)

re:	fclean all

.PHONY:	all fclean re

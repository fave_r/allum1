/*
** allum1.h for Allum1 in /home/blackbird/work/Allum1
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Fri Feb  7 18:30:23 2014 romaric
** Last update Sun Feb 16 18:52:14 2014 romaric
*/

#ifndef __allum1__
#define __allum1__

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <term.h>
#include <curses.h>
#include <sys/wait.h>

int     my_strlen(char *str);
int     my_putstr(char *str, int op);
void    *xmalloc(size_t n);
char    *rmterm(char *dest, char * src);
char    *termcpy(char **env, int i);
int      checkenvterm(char **env);
char	*checterm(char **env);
int     my_strncmp(char *s1, char *s2, int n);
int     my_putint(int i);
void    auxmain(char **env);
void    xtgetent(char *term);
void	xtcgetattr(int fd, void *t);
void	xtcsetattr(int fd, int opt, void *t);
void    my_bzero(char *s, size_t n);
void	myclear();
void    grepenter(char *buffer);
void	grepesc(char *buffer, int len);
void	iniint(int **alum);
void	display(int *alum);
void	init(int *i, int *y, int *z, int *x);
void	loopgame(int *alum);
ssize_t	xread(int fd, void *buf, size_t count);
int     is_negative(char *str);
int     is_nume(char l);
int     my_getnbr(char *str);
void    removeint(char *line, char *nbr, int *alum);
void	computertour(int *alum);
int     suitxor(int *nbr, int *alum, int X);
void    testuwin(int *alum);
void    checkiawin(int *alum);
void    removefirstalum(int *alum);
int     checkonealumdeck(int *alum);
int     computeX(int *alum);

#endif

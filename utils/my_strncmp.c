/*
** my_strncmp.c for Allum1 in /home/blackbird/rendu/Allum1
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Sun Feb 16 18:41:06 2014 romaric
** Last update Sun Feb 16 18:56:13 2014 romaric
*/

#include "../headers/allum1.h"

int	my_strncmp(char *s1, char *s2, int n)
{
  int	i;

  i = 0;
  while (i < n)
    {
      if (s1[i] == '\0' && s2[i] == '\0')
	return (0);
      if (s1[i] < s2[i] && i < n)
	return (-1);
      if (s1[i] > s2[i] && i < n)
	return (1);
      i++;
    }
  return (0);
}

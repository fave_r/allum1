/*
** my_getnbr.c for Allum1 in /home/blackbird/rendu/Allum1
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Sun Feb 16 18:43:18 2014 romaric
** Last update Sun Feb 16 18:52:35 2014 romaric
*/

#include "../headers/allum1.h"

int     my_getnbr(char *str)
{
  int   cpt;
  int   i;
  int   result;

  result = 0;
  i = 0;
  cpt = 1;
  while (str[i] >= 48 && str[i] <= 57)
    {
      cpt = cpt * 10;
      i++;
    }
  cpt = cpt / 10;
  i = 0;
  while (str[i] >= 48 && str[i] <= 57)
    {
      result = result + ((str[i] - 48) * cpt);
      cpt = cpt / 10;
      i++;
    }
  return (result);
}

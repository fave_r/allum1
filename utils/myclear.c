/*
** myclear.c for Allum1 in /home/blackbird/rendu/Allum1
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Sun Feb 16 18:40:38 2014 romaric
** Last update Sun Feb 16 18:55:32 2014 romaric
*/

#include "../headers/allum1.h"

void	myclear()
{
  char	*str;

  str = tgetstr("cl", NULL);
  tputs(str, 1, my_putint);
}

/*
** char.c for Allum1 in /home/blackbird/work/Allum1
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Fri Feb  7 18:30:47 2014 romaric
** Last update Sun Feb 16 18:48:02 2014 romaric
*/

#include "../headers/allum1.h"

int     my_strlen(char *str)
{
  int   x;

  x = 0;
  if (str != NULL)
    {
      while (str[x] != '\0' && str[x] != '\n')
	x++;
      return (x + 1);
    }
  return (0);
}

int     my_putstr(char *str, int op)
{
  return (write(op, str, my_strlen(str)));
}

int	my_putint(int	i)
{
  return (write(1, &i, 1));
}

/*
** xfunction.c for Allum1 in /home/blackbird/work/Allum1
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Fri Feb  7 18:34:04 2014 romaric
** Last update Sun Feb 16 18:54:02 2014 romaric
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "../headers/allum1.h"

ssize_t xread(int fd, void *buf, size_t count)
{
  ssize_t	len;

  len = read(fd, buf, count);
  if (len == -1)
    {
      my_putstr("\033[31mread fail !\033[0;m", 2);
      exit(EXIT_FAILURE);
    }
  return (len);
}

void	*xmalloc(size_t n)
{
  void	*mal;

  mal = malloc(n);
  if (mal == NULL)
    {
      my_putstr("\033[31mmalloc fail !\033[0;m", 2);
      exit(EXIT_FAILURE);
    }
  return (mal);
}

void	xtgetent(char *term)
{
  int	i;

  i = tgetent(NULL, term);
  if (i != 1)
    {
      my_putstr("\033[31mtgetent fail !\033[0;m", 2);
      exit(EXIT_FAILURE);
    }
}

void	xtcgetattr(int fd, void *t)
{
  int	i;

  i = tcgetattr(fd, t);
  if (i == -1)
    {
      my_putstr("\033[31mtcgetattr fail !\033[0;m", 2);
      exit(EXIT_FAILURE);
    }
}

void	xtcsetattr(int fd, int opt, void *t)
{
  int	i;

  i = tcsetattr(fd, opt, t);
  if (i == -1)
    {
      my_putstr("\033[31mtcsetattr fail !\033[0;m", 2);
      exit(EXIT_FAILURE);
    }
}

/*
** display.c for Allum1 in /home/blackbird/rendu/Allum1
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Sun Feb 16 18:39:47 2014 romaric
** Last update Sun Feb 16 18:56:53 2014 romaric
*/

#include "../headers/allum1.h"

void	display(int *alum)
{
  int	i;
  int	x;
  int	y;
  int	z;

  init(&i, &y, &z, &x);
  while (i <= 5)
    {
      while (y != x)
	{
	  my_putstr(" ", 1);
	  y++;
	}
      x = x - 1;
      y = 0;
      while (z != alum[i])
	{
	  my_putstr("|", 1);
	  z++;
	}
      z = 0;
      i++;
      write(1, "\n", 1);
    }
}

void	init(int *i, int *y, int *z, int *x)
{
  *i = 0;
  *y = 0;
  *x = 5;
  *z = 0;
}

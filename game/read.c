/*
** read.c for Allum1 in /home/blackbird/rendu/Allum1
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Sun Feb 16 18:41:29 2014 romaric
** Last update Sun Feb 16 19:59:05 2014 romaric
*/

#include "../headers/allum1.h"

void	loopgame(int *alum)
{
  char	*line;
  char	*nbr;

  line = xmalloc(3 * sizeof(char));
  nbr = xmalloc(3 * sizeof(char ));
  while (42)
  {
    myclear();
    display(alum);
    my_putstr("\033[34mline:\033[0;m", 1);
    xread(0, line, 4096);
    line[2] = '\0';
    my_putstr("\033[34mnbr:\033[0;m", 1);
    xread(0, nbr, 4096);
    nbr[2] = '\0';
    removeint(line, nbr, alum);
    checkiawin(alum);
    myclear();
    display(alum);
    computertour(alum);
  }
}

void	removeint(char *line, char *nbr, int *alum)
{
  int	linei;
  int	nbri;

  linei = my_getnbr(line);
  nbri = my_getnbr(nbr);
  if (nbri > alum[linei - 1] || linei > 6 || nbri <= 0 || linei <= 0)
    {
      my_putstr("\033[31mFAKE LINE or NBR!\033[0;m", 2);
      return (loopgame(alum));
    }
  else
    linei--;
  alum[linei] = alum[linei] - nbri;
}

void	checkiawin(int *alum)
{
    if (alum[0] == 0 && alum[1] == 0 && alum[2] == 0
	&& alum[3] == 0 && alum[4] == 0 && alum[5] ==0)
      {
	my_putstr("\033[31mIA WIN !\n\033[0;m", 1);
	exit(EXIT_SUCCESS);
      }
}

/*
** IA.c for Allum1 in /home/blackbird/rendu/Allum1
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Sun Feb 16 18:42:21 2014 romaric
** Last update Wed Feb 19 09:50:52 2014 romaric
*/

#include "../headers/allum1.h"

int	computeX(int *alum)
{
  int	X;

  X = alum[0] ^ alum[1] ^ alum[2] ^ alum[3] ^ alum[4] ^ alum[5];
  return (X);
}

void	removefirstalum(int *alum)
{
  int	i;

  i = 0;
  while (alum[i] == 0)
    i++;
  alum[i] = alum[i] - 1;
}

int	checkonealumdeck(int *alum)
{
  int	i;
  int	nbr;

  nbr = 0;
  i = 0;
  while (i <= 5)
    {
      if (alum[i] == 1)
	nbr++;
      i++;
    }
  return (nbr);
}

void	computertour(int *alum)
{
  int	i;
  int	X;

  X = computeX(alum);
  i = 0;
  if (X == 0)
    removefirstalum(alum);
  else
    {
      i = 0;
      while ((alum[i] ^ X) > alum[i])
	++i;
      if (checkonealumdeck(alum) % 2 != 0 && (alum[i] ^ X) == 1)
	alum[i] = 0;
      else if (checkonealumdeck(alum) % 2 == 0 && (alum[i] ^ X) == 0)
	alum[i] = 1;
      else
	alum[i] ^= X;
      myclear();
      display(alum);
      testuwin(alum);
    }
}

void	testuwin(int *alum)
{
  if (alum[0] == 0 && alum[1] == 0 && alum[2] == 0
      && alum[3] == 0 && alum[4] == 0 && alum[5] ==0)
    {
      my_putstr("\033[31mU WIN !\n\033[0;m", 1);
      exit(EXIT_SUCCESS);
    }
}
